import $ivy.`com.github.tototoshi::scala-csv:1.3.10`
import $ivy.`io.circe::circe-core:0.14.4`
import $ivy.`io.circe::circe-generic:0.14.4`
// import $ivy.`io.circe::circe-parser:0.14.4`

import io.circe.generic.auto._
import io.circe.syntax._

import scala.util.Try
import com.github.tototoshi.csv.CSVReader

case class Row(
  `type`: Int,
  text: String,
  range: List[Int],
  weight: Int = 1,
  draw: Boolean = false,
  img: Option[String] = Some("icons/svg/d20-black.svg")
)

case class Table(
  name: String,
  results: List[Row],
  description: Option[String] = None,
  img: Option[String] = Some("icons/svg/d20-grey.svg"),
  formula: Option[String] = None
)

def strToOption(s: String) = if(s.trim.isEmpty) None else Option(s.trim)

@main
def main(csvPath: os.Path, out: Option[os.Path]) = {
  val csv = CSVReader.open(csvPath.toIO).toStream.drop(1)
  val tableDef = csv.head
  val rows = csv.drop(2).toList


  val tableRows = rows.map{ case rowType :: text :: weight :: from :: to :: Nil => 
    Row(
      rowType.toInt,
      text,
      range = List(from.toInt, to.toInt),
      weight = Try(weight.toInt).getOrElse(1),
      draw = false
    )
  }

  val name :: description :: image :: formula :: replacement :: Nil = tableDef
  val table = Table(
    name,
    tableRows,
    strToOption(description),
    strToOption(image).orElse(Some("icons/svg/d20-grey.svg")),
    strToOption(formula)
  )
  
  println(table.asJson.spaces2)

}
