#!/usr/bin/env python3

import sys
import csv

TABLE_TEMPLATE = """
{
  "name": "%{tableName}s",
  "description": "%{tableDescription}s",
  "results": [
    ${results}s
  ],
  "replacement": %{tableReplacement}s,
  "displayRoll": %{tableDisplayRoll}s,
  "img": "%{tableImage}s",
  "formula": "%{tableFormula}s",
}
"""

RESULT_TEMPLATE = """
{
  "type": %{resultType}d,
  "text": "%{resultText}s",
  "weight": %{resultWeight}d,
  "range": [
    %{resultFrom}d,
    %{resultTo}d
  ],
  "drawn": %{resultDrawn},
  "img": "%{resultImage}"
}
"""

EMPTY_STRING = ""
TABLE_IMG = "icons/svg/d20-grey.svg"
RESULT_TYPE = 0
RESULT_WEIGHT = 1
RESULT_DRAWN = "false"

