const COMPENDIUM_KEY = 'mz-addons.mz-addons-tables'

export async function getTable(tableName) {
  const pack = game.packs.get(COMPENDIUM_KEY);
  const index = await pack.getIndex()
  const entry = index.find(e => e.name === tableName);
  return pack.getDocument(entry._id);
}

export async function preloadTemplates() {
  const partials = [
    "modules/mz-addons/templates/extended-reaction.html"
  ]

  return loadTemplates(partials)
}

/**
 *
 * @param {any[]} source
 * @param {bigint} numberOfElements
 */
export function getRandomElements(source, numberOfElements) {
  function run(remainElements, choosenElements, numberOfElements) {
    if (remainElements.length > 0 && numberOfElements > 0) {
      const elem = remainElements[Math.random() * remainElements.length | 0]
      const remains = remainElements.filter(e => e != elem)
      return run(remains, choosenElements.concat(elem), numberOfElements - 1)
    } else return choosenElements
  }

  return run(source, [], numberOfElements)
}