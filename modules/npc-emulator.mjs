import {getTable, getRandomElements} from "./utils.mjs";


const defaultParams = {
  npcPower: "Standard", numberOfMotivations: 3
}

export async function generateEmulatorNPC(params) {

  const generationParams = params ? Object.assign(defaultParams, params) : defaultParams
  const modifiersTable = await getTable("NPC Modifier")
  const nounsTable = await getTable("NPC Noun")
  const powerTable = await getTable(`NPC Power Level ${generationParams.npcPower}`)
  const nounsNumbers = getRandomElements(
    [1, 2, 3, 4, 5],
    generationParams.numberOfMotivations
  )
  const motivationNounsTables = await Promise.all(
    nounsNumbers.map((motive) => getTable(`NPC Motivation Noun ${motive}`))
  )
  const motivationVerbsTable = await getTable("NPC Motivation Verb")

  const modifierRoll = await modifiersTable.roll()
  const nounRoll = await nounsTable.roll()
  const powerRoll = await powerTable.roll()
  const motivationsRolls = await Promise.all(
    motivationNounsTables.map(mnTable => {
      const motivationNounRoll = mnTable.roll()
      const motivationVerbRoll = motivationVerbsTable.roll()
      return Promise.all([motivationVerbRoll, motivationNounRoll])
    })
  )

  const motivations = motivationsRolls
    .map(([vRoll, nRoll]) => `&nbsp;-&nbsp;${vRoll.results[0].text} ${nRoll.results[0].text}`)
    .join("<br>")

  const message = `
  NPC: ${modifierRoll.results[0].text} ${nounRoll.results[0].text}<br>
  Motivations:<br>
  ${motivations}<br>
  Power: ${powerRoll.results[0].text}
  `

  const messageData = {
    content: message
  }

  ChatMessage.create(messageData)
}