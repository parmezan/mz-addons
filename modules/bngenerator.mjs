
function generateName(gender) {
    const nameGenUrl = "https://namey.muffinlabs.com/name.json?count=1&type=" + gender + "&with_surname=true&frequency=rare";


    return new Promise((resolve, reject) => {

        const http = new XMLHttpRequest();
        http.open("GET", nameGenUrl);

        http.onload = () => {
            if (http.status >= 200 && status < 300) {
                resolve(JSON.parse(http.responseText)[0]);
            } else {
                reject(http.statusText);
            }
        }

        http.onerror = () => {
            reject(http.statusText);
        }

        http.send(null);


    })
};

async function getDescriptionTables() {
    const pack = game.packs.get("mz-addons.mz-addons-tables");
    const lookTable = await pack.getDocument("NJY8foioQPXrzl2K");
    const personalityTable = await pack.getDocument("pYMbfMQQqEhCYevo");
    return {
        look: lookTable,
        personality: personalityTable
    }
}

async function getDescription() {

    const descTables = await getDescriptionTables();
    const lookRoll = await descTables.look.roll();
    const look = lookRoll.results[0].text;
    const personalityRoll = await descTables.personality.roll();
    const personality = personalityRoll.results[0].text;

    return `${look} i ${personality}`;
}

function generateBN(gender) {

    generateName(gender).then(name => {
        getDescription().then(description => {
            const message = name + " " + description;

            ChatMessage.create({
                user: game.user.id,
                speaker: ChatMessage.getSpeaker(),
                content: message,
                whisper: [game.userId]
            })
        });
    });
}

function generateRandomBN() {
    if (Math.floor(Math.random() * 2) > 0) {
        generateBN('male');
    } else {
        generateBN('female');
    }
}

function generateNPCDialog() {

    const d = new Dialog({
        title: "Losowanie BNa",
        content: "Wybierz płeć",
        buttons: {
            female: {
                icon: '<i class="fas fa-female"></i>',
                label: "Kobieta",
                callback: () => generateBN('female')
            },
            male: {
                icon: '<i class="fas fa-male"></i>',
                label: "Mężczyzna",
                callback: () => generateBN('male')
            },
            random: {
                icon: '<i class="fas fa-random"></i>',
                label: "Losuj",
                callback: () => generateRandomBN()
            }
        },
        default: "random",
    });
    d.render(true);
}

export default generateNPCDialog