
import { getTable } from "./utils.mjs"

async function drawFromTable(tableName) {
    const table = await getTable(tableName);
    table.draw();
}

async function rollFromTable(tableName) {
    const table = await getTable(tableName);
    return table.roll();
}

export async function askOracle() {
    drawFromTable("Wyrocznia");
}

async function rollTwoTables(firstName, secondName) {
    const title = `${firstName} + ${secondName}`;
    const getText = (roll) => roll?.results[0]?.text;
    const fResult = await rollFromTable(firstName).then(getText);
    const sResult = await rollFromTable(secondName).then(getText);
    const content = `<i>${title}</i>:<br>${fResult} ${sResult}`;
    const msgData = {
        content: content,
        type: CONST.CHAT_MESSAGE_TYPES.OOC,
        whisper: [game.userId]
    };
    return ChatMessage.create(msgData);
}

export async function actionPlusTheme() {
    return await rollTwoTables("Action", "Theme");
}

export async function descriptorPlusFocus() {
    return await rollTwoTables("Descriptor", "Focus");
}