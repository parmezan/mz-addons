import { getTable } from "./utils.mjs";

export async function extendedReaction() {

  const reactionTable = await getTable("Extended Reactions")
  const tableRoll = await reactionTable.roll()
  const roll = tableRoll.roll
  const result = tableRoll.results[0]

  const [category, detail] = result.text.split(": ")
  const img = result.img
  const renderContext = {
    icon: result.img,
    category: category,
    reaction: detail
  }

  const msg = await renderTemplate("modules/mz-addons/templates/extended-reaction.html", renderContext)
  const renderedRoll = await roll.render()

  const messageData = {
    rolls: [roll],
    content: msg + renderedRoll,
    type: CONST.CHAT_MESSAGE_TYPES.ROLL
  }
  
  ChatMessage.create(messageData)
}