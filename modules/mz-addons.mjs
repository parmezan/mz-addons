
import { askOracle, actionPlusTheme, descriptorPlusFocus } from "./oracle.mjs";
import generateNPC from "./bngenerator.mjs"
import { preloadTemplates } from "./utils.mjs"
import { extendedReaction } from "./reactions.mjs";
import { generateEmulatorNPC } from "./npc-emulator.mjs"


Hooks.on("init", function () {

    const functions = {
        askOracle: askOracle,
        actionPlusTheme: actionPlusTheme,
        descriptorPlusFocus: descriptorPlusFocus,
        generateNPC: generateNPC,
        extendedReaction: extendedReaction,
        generateEmulatorNPC: generateEmulatorNPC
    }

    game.mzaddons = functions;

    preloadTemplates();
    console.log("MZ Addons initialized");
});