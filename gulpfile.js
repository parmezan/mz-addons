const gulp = require('gulp');
const zip = require('gulp-zip');
const clean = require('gulp-clean');

gulp.task('zip', () => {
    return gulp.src([
        "packs/**",
        "icons/**",
        "modules/**",
        "css/**",
        "templates/**",
        "assets/**",
        "module.json"], {base: ".."})
        .pipe(zip('mz-addons.zip'))
        .pipe(gulp.src('module.json'))
        .pipe(gulp.dest('dist/'));
});

gulp.task('clean', () => {
    return gulp.src('./dist', {read: false}).pipe(clean());
});
